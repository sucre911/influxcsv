package de.influxcsv.influxdb.data;

import org.junit.Test;

import java.security.InvalidParameterException;

import static org.junit.Assert.assertEquals;

public class MeasurementLineWriteTest {

    @Test(expected=InvalidParameterException.class)
    public void testLineWriteNoField() {
        final Measurement m = new Measurement("test");
        m.toString();
    }

    @Test
    public void testLineWriteOneField() {
        final Measurement m = new Measurement("test");
        m.addField(new Field("field1key", "field1value"));
        assertEquals("test field1key=\"field1value\"", m.toString());
    }

    @Test
    public void testLineWriteOneBooleanField() {
        final Measurement m = new Measurement("test");
        m.addField(new Field("field1key", Boolean.TRUE));
        assertEquals("test field1key=true", m.toString());
    }

    @Test
    public void testLineWriteOneFieldWithTimestamp() {
        final Measurement m = new Measurement("test", "1518617114196980828");
        m.addField(new Field("field1key", "field1value"));
        assertEquals("test field1key=\"field1value\" 1518617114196980828", m.toString());
    }

    @Test
    public void testLineWriteMultipleFields() {
        final Measurement m = new Measurement("test");

        m.addField(new Field("field1key", "field 1 value"));
        m.addField(new Field("field2key", 2));
        m.addField(new Field("field3key", 3.0));
        m.addField(new Field("field 4 key", 4.0));

        assertEquals("test field1key=\"field 1 value\",field2key=2i,field3key=3.0,field\\ 4\\ key=4.0", m.toString());
    }

    @Test
    public void testLineWriteTag() {
        final Measurement m = new Measurement("test");

        m.addField(new Field("field1key", 1));
        m.addTag(new Tag("tag1key", "tag1value"));

        assertEquals("test,tag1key=tag1value field1key=1i", m.toString());
    }

    @Test(expected=InvalidParameterException.class)
    public void testLineWriteFieldKeyNull() {
        final Measurement m = new Measurement("test");

        m.addField(new Field(null, 1));
    }

    @Test(expected=InvalidParameterException.class)
    public void testLineWriteTagKeyNull() {
        final Measurement m = new Measurement("test");

        m.addTag(new Tag(null, "tag1value"));
    }

    @Test
    public void testLineWriteTagWithSpaces() {
        final Measurement m = new Measurement("test");

        m.addField(new Field("field1key", 1));
        m.addTag(new Tag("tag 1 key", "tag 1 value"));

        assertEquals("test,tag\\ 1\\ key=tag\\ 1\\ value field1key=1i", m.toString());
    }

    @Test
    public void testLineWriteTagWithNullTag() {
        final Measurement m = new Measurement("test");

        m.addField(new Field("field1key", 1));
        m.addTag(new Tag("tag1key", null));

        assertEquals("test,tag1key=null field1key=1i", m.toString());
    }

    @Test
    public void testLineWriteTagWithNullField() {
        final Measurement m = new Measurement("test");

        final Integer i = null;
        m.addField(new Field("field1key", i));
        m.addTag(new Tag("tag1key", "tag1value"));

        assertEquals("test,tag1key=tag1value field1key=\"null\"", m.toString());
    }

    @Test
    public void testLineWriteMultipleTagsWithSpaces() {
        final Measurement m = new Measurement("test");

        m.addField(new Field("field1key", 1));
        m.addTag(new Tag("tag 1 key", "tag 1 value"));
        m.addTag(new Tag("tag2key", "tag 2 value"));
        m.addTag(new Tag("tag 3 key", "tag3value"));
        m.addTag(new Tag("tag4key", "tag4value"));

        assertEquals("test,tag\\ 1\\ key=tag\\ 1\\ value,tag2key=tag\\ 2\\ value,tag\\ 3\\ key=tag3value,tag4key=tag4value field1key=1i", m.toString());
    }

    @Test
    public void testLineWriteNameWithSpaces() {
        final Measurement m = new Measurement("test test");

        m.addField(new Field("field1key", 1));

        assertEquals("test\\ test field1key=1i", m.toString());
    }

}
