package de.influxcsv.parser;

import org.junit.Test;

public class InfluxCSVParserTest {
    private static final String[] EMPTRY_STRINGS = {};
    private InfluxCSVParser influxCSVParser = new InfluxCSVParser();

    @Test
    public void parse() throws Exception {
        final String file = this.getClass().getClassLoader().getResource("climateV2.csv").getFile();
        influxCSVParser.parse(file, EMPTRY_STRINGS, EMPTRY_STRINGS, EMPTRY_STRINGS, EMPTRY_STRINGS);
    }

}