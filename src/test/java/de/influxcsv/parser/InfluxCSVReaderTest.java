package de.influxcsv.parser;

import org.junit.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class InfluxCSVReaderTest {
    private final String TEST_FILE = this.getClass().getClassLoader().getResource("climateV2.csv").getFile();
    private InfluxCSVReader influxCSVReader = new InfluxCSVReader();

    @Test
    public void testParse() throws IOException {
        final List<String[]> lines = influxCSVReader.parse(TEST_FILE);
        assertEquals(14, lines.size());
        final String[] head = lines.get(0);
        assertEquals(9, head.length);
        assertEquals("[name, time, battery, humidity, name, sensor, temperature, temperature2, value]", Arrays.toString(head));
    }

}
