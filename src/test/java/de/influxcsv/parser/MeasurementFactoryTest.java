package de.influxcsv.parser;

import de.influxcsv.influxdb.data.Field;
import de.influxcsv.influxdb.data.Measurement;
import de.influxcsv.influxdb.data.Tag;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class MeasurementFactoryTest {
    private static final String[] EMPTY_STRINGS = {};
    private MeasurementFactory measurementFactory = new MeasurementFactory();
    private InfluxCSVReader influxCSVReader = new InfluxCSVReader();
    private List<String[]> lines;

    @Before
    public void setUp() throws Exception {
        final String file = this.getClass().getClassLoader().getResource("climateV2.csv").getFile();
        lines = influxCSVReader.parse(file);
    }

    @Test
    public void testLine() throws Exception {
        final Measurement m = measurementFactory.buildMeasurement(lines.get(0), lines.get(1));
        assertNotNull(m);
        assertEquals("climateV2", m.getName());
        assertEquals("1518541255124111178", m.getTimestamp());

        final List<Tag> tags = m.getTags().stream().collect(Collectors.toList());
        assertEquals(2, tags.size());
        assertEquals("battery", tags.get(0).getKey());
        assertEquals("sensor", tags.get(1).getKey());

        final List<Field> fields = m.getFields().stream().collect(Collectors.toList());
        assertEquals(2, fields.size());
        assertEquals("humidity", fields.get(0).getKey());
        assertTrue(fields.get(0).getValue() instanceof Integer);
        assertEquals("temperature", fields.get(1).getKey());
        assertTrue(fields.get(1).getValue() instanceof Double);
    }

    @Test
    public void testLine7() throws Exception {
        final Measurement m = measurementFactory.buildMeasurement(lines.get(0), lines.get(7));
        assertNotNull(m);
        assertEquals("climateV2", m.getName());
        assertEquals("1518613542526373669", m.getTimestamp());

        final List<Tag> tags = m.getTags().stream().collect(Collectors.toList());
        assertEquals(2, tags.size());
        assertEquals("battery", tags.get(0).getKey());
        assertEquals("sensor", tags.get(1).getKey());

        final List<Field> fields = m.getFields().stream().collect(Collectors.toList());
        assertEquals(4, fields.size());
        assertEquals("humidity", fields.get(0).getKey());
        assertEquals("temperature", fields.get(1).getKey());
        assertEquals("temperature2", fields.get(2).getKey());
        assertEquals("value", fields.get(3).getKey());
    }

    @Test
    public void testLine8() throws Exception {
        final Measurement m = measurementFactory.buildMeasurement(lines.get(0), lines.get(8));
        assertNotNull(m);
        assertEquals("climateV2", m.getName());
        assertEquals("1518617114196980828", m.getTimestamp());

        final List<Tag> tags = m.getTags().stream().collect(Collectors.toList());
        assertEquals(3, tags.size());
        assertEquals("battery", tags.get(0).getKey());
        assertEquals("name", tags.get(1).getKey());
        assertEquals("sensor", tags.get(2).getKey());

        final List<Field> fields = m.getFields().stream().collect(Collectors.toList());
        assertEquals(3, fields.size());
        assertEquals("humidity", fields.get(0).getKey());
        assertEquals("temperature", fields.get(1).getKey());
        assertEquals("value", fields.get(2).getKey());
    }

    @Test
    public void testLineForceFloat() throws Exception {
        measurementFactory = new MeasurementFactory(new String[]{"humidity"}, EMPTY_STRINGS, EMPTY_STRINGS, EMPTY_STRINGS);
        final Measurement m = measurementFactory.buildMeasurement(lines.get(0), lines.get(1));
        assertNotNull(m);
        final List<Field> fields = m.getFields().stream().collect(Collectors.toList());
        assertEquals(2, fields.size());
        assertEquals("humidity", fields.get(0).getKey());
        assertTrue(fields.get(0).getValue() instanceof Double);
    }

    @Test
    public void testLineForceInt() throws Exception {
        measurementFactory = new MeasurementFactory(EMPTY_STRINGS, new String[]{"temperature"}, EMPTY_STRINGS, EMPTY_STRINGS);
        final Measurement m = measurementFactory.buildMeasurement(lines.get(0), lines.get(2));
        assertNotNull(m);
        final List<Field> fields = m.getFields().stream().collect(Collectors.toList());
        assertEquals(2, fields.size());
        assertEquals("temperature", fields.get(1).getKey());
        assertTrue(fields.get(1).getValue() instanceof Integer);
    }

    @Test
    public void testLineForceBoolean() throws Exception {
        measurementFactory = new MeasurementFactory(EMPTY_STRINGS, EMPTY_STRINGS, new String[]{"value"}, EMPTY_STRINGS);
        final Measurement m = measurementFactory.buildMeasurement(lines.get(0), lines.get(8));
        assertNotNull(m);
        final List<Field> fields = m.getFields().stream().collect(Collectors.toList());
        assertEquals(3, fields.size());
        assertEquals("value", fields.get(2).getKey());
        assertTrue(fields.get(2).getValue() instanceof Boolean);
    }
    @Test
    public void testLineForceString() throws Exception {
        measurementFactory = new MeasurementFactory(EMPTY_STRINGS, EMPTY_STRINGS, EMPTY_STRINGS, new String[]{"temperature"});
        final Measurement m = measurementFactory.buildMeasurement(lines.get(0), lines.get(1));
        assertNotNull(m);
        final List<Tag> tags = m.getTags().stream().collect(Collectors.toList());
        assertEquals(3, tags.size());
        assertEquals("temperature", tags.get(2).getKey());
        assertTrue(tags.get(1).getValue() instanceof String);
    }
}