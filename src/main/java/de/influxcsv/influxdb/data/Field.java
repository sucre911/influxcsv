package de.influxcsv.influxdb.data;

import static de.influxcsv.constants.InfluxDBConstants.*;

public class Field extends AbstractKeyValuePair {

    public Field(final String key, final Integer value) {
        super(key, value);
    }
    public Field(final String key, final Double value) {
        super(key, value);
    }
    public Field(final String key, final Boolean value) { super(key, value); }
    public Field(final String key, final String value) { super(key, value); }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();

        sb.append(this.key.replaceAll(SPACE, ESCAPED_SPACE));
        sb.append(EQUAL);

        if (this.value == null) {
            sb.append(QUOTED_NULL);
        } else if (this.value instanceof Double) {
            sb.append(this.value.toString());
        } else if (this.value instanceof Integer) {
            sb.append(this.value.toString());
            sb.append(INTEGER);
        }else if (this.value instanceof Boolean) {
            sb.append((Boolean)this.value ? BOOLEAN_TRUE : BOOLEAN_FALSE);
        } else if (this.value instanceof String) {
            sb.append(QUOTE);
            sb.append(this.value);
            sb.append(QUOTE);
        }
        return sb.toString();
    }

}
