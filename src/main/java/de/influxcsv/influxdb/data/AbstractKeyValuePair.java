package de.influxcsv.influxdb.data;

import java.security.InvalidParameterException;

public abstract class AbstractKeyValuePair {

    protected final String key;
    protected final Object value;

    protected AbstractKeyValuePair(final String key, final Object value) throws InvalidParameterException {
        if (key == null) {
            throw new InvalidParameterException("key must not be null");
        }
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public abstract String toString();
}
