package de.influxcsv.influxdb.data;

import java.security.InvalidParameterException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

import static de.influxcsv.constants.InfluxDBConstants.COMMA;
import static de.influxcsv.constants.InfluxDBConstants.SPACE;

public class Measurement {

    private final String name;
    private final String timestamp;
    private final Set<Tag> tags = new LinkedHashSet<>();
    private final Set<Field> fields = new LinkedHashSet<>();

    public Measurement(final String name) {
        this.name = name;
        this.timestamp = null;
    }

    public Measurement(final String name, final String timestamp) {
        this.name = name;
        this.timestamp = timestamp;
    }

    public String getName() {
        return this.name;
    }

    public String getTimestamp() { return timestamp; }

    public Set<Tag> getTags() {
        return this.tags;
    }

    public void addTag(final Tag tag) {
        this.tags.add(tag);
    }

    public Set<Field> getFields() {
        return this.fields;
    }

    public void addField(final Field field) {
        this.fields.add(field);
    }

    @Override
    public String toString() {
        if (this.getFields().size() < 1) {
            throw new InvalidParameterException("InfluxDB measurement must have at least one field.");
        }

        final StringBuilder sb = new StringBuilder();

        sb.append(this.name.replaceAll(SPACE, "\\\\ "));

        for (final Tag tag : this.tags) {
            sb.append(COMMA);
            sb.append(tag);
        }

        sb.append(SPACE);

        final Iterator<Field> fieldsIterator = this.fields.iterator();
        sb.append(fieldsIterator.next());
        while (fieldsIterator.hasNext()) {
            sb.append(COMMA);
            sb.append(fieldsIterator.next());
        }

        if(timestamp != null) {
            sb.append(SPACE);
            sb.append(timestamp);
        }

        return sb.toString();
    }

}
