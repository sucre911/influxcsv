package de.influxcsv.influxdb.data;

import static de.influxcsv.constants.InfluxDBConstants.*;

public class Tag extends AbstractKeyValuePair {

    public Tag(final String key, final String value) {
        super(key, value);
    }

    @Override
    public String toString() {
        return (this.key + EQUAL + (this.value == null ? NULL : this.value))
                .replaceAll(SPACE, ESCAPED_SPACE);
    }

}
