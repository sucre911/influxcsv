package de.influxcsv;

import de.influxcsv.parser.InfluxCSVParser;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);

    private static final CommandLineParser commandLineParser = new PosixParser();

    private static final String HELP_DESC = "InfluxDB CSV export to \"Line Protocol\" converter: ";
    private static final String FORCE_TYPE_DESC = "(optional) header names which force interpreted ";
    private static final String CSV_FILE_DESC = "file which should converted from csv to line protocol";
    private static final String OPTION_AS_FLOAT = "asFloat";
    private static final String OPTION_AS_INT = "asInt";
    private static final String OPTION_AS_BOOLEAN = "asBoolean";
    private static final String OPTION_AS_STRING = "asString";
    private static final String OPTION_CSV = "csv";

    public static void main(String[] args) throws IOException {
        final Options options = buildOptions();

        try {
            final CommandLine res = commandLineParser.parse(options, args);

            new InfluxCSVParser().parse(res.getOptionValue(OPTION_CSV),
                    res.getOptionValues(OPTION_AS_FLOAT),
                    res.getOptionValues(OPTION_AS_INT),
                    res.getOptionValues(OPTION_AS_BOOLEAN),
                    res.getOptionValues(OPTION_AS_STRING));
        } catch (ParseException e) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(HELP_DESC, options);
            System.out.println("\n\nWarning: " + e.getMessage());
        }
    }

    private static Options buildOptions() {
        final Options options = new Options();

        final Option csv = new Option(OPTION_CSV, true, CSV_FILE_DESC);
        csv.setRequired(true);
        options.addOption(csv);

        final OptionGroup group = new OptionGroup();
        group.addOption(buildTypeOption(OPTION_AS_FLOAT));
        group.addOption(buildTypeOption(OPTION_AS_INT));
        group.addOption(buildTypeOption(OPTION_AS_BOOLEAN));
        group.addOption(buildTypeOption(OPTION_AS_STRING));
        options.addOptionGroup(group);

        return options;
    }

    private static Option buildTypeOption(final String type) {
        return new Option(type, true, FORCE_TYPE_DESC + type);
    }

}
