package de.influxcsv.constants;

public interface InfluxDBConstants {
    String SPACE = " ";
    String ESCAPED_SPACE = "\\\\ ";
    String COMMA = ",";
    String EQUAL = "=";
    String NULL = "null";
    String QUOTE = "\"";
    String QUOTED_NULL = QUOTE + NULL + QUOTE;
    String INTEGER = "i";
    String BOOLEAN_TRUE = "true";
    String BOOLEAN_FALSE = "false";
}