package de.influxcsv.parser;

import au.com.bytecode.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class InfluxCSVReader {
    private static final Logger LOG = LoggerFactory.getLogger(InfluxCSVReader.class);
    public List<String[]> parse(final String filename) throws IOException {
        final File file = new File(filename);
        final CSVReader reader = new CSVReader(new FileReader(file),',', '"');
        final List<String[]> lines = reader.readAll();
        if(LOG.isDebugEnabled()) {
            lines.forEach(e -> LOG.debug(Arrays.toString(e)));
        }
        return lines;
    }
}
