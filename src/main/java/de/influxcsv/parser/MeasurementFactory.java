package de.influxcsv.parser;

import de.influxcsv.influxdb.data.Field;
import de.influxcsv.influxdb.data.Measurement;
import de.influxcsv.influxdb.data.Tag;

import java.util.Arrays;
import java.util.regex.Pattern;

public class MeasurementFactory {

    private class Triplet {
        final Measurement measurement;
        final String headValue;
        final String lineValue;

        public Triplet(final Measurement measurement, final String headValue, final String lineValue) {
            this.measurement = measurement;
            this.headValue = headValue;
            this.lineValue = lineValue;
        }
    }

    private final static Pattern intPattern = Pattern.compile("^-?[0-9]+$");
    private final static Pattern floatPattern = Pattern.compile("^-?[0-9]*\\.[0-9]*$");
    private final static Pattern booleanPattern = Pattern.compile("^(true|false)$");
    private final String[] asFloats;
    private final String[] asInt;
    private final String[] asBoolean;
    private final String[] asString;

    public MeasurementFactory() {
        asFloats = null;
        asInt = null;
        asBoolean = null;
        asString = null;
    }

    public MeasurementFactory(final String[] asFloats, final String[] asInt, final String[] asBoolean, final String[] asString) {
        this.asFloats = asFloats;
        this.asInt = asInt;
        this.asBoolean = asBoolean;
        this.asString = asString;
    }

    public Measurement buildMeasurement(final String[] head, final String[] line) {
        final Measurement m = new Measurement(line[0], line[1]);
        for(int i=2;i<line.length;i++) {
            final Triplet t = new Triplet(m, head[i], line[i]);
            if (!line[i].isEmpty()) {
                if(isForceFloat(t.headValue)) {
                    addFloatField(t);
                } else if(isForceInt(t.headValue)) {
                    addIntField(t);
                } else if(isForceBoolean(t.headValue)) {
                    addBooleanField(t);
                } else if(isForceString(t.headValue)) {
                    addTag(t);
                } else if(isInteger(t.lineValue)) {
                    addIntField(t);
                } else if(isFloat(t.lineValue)){
                    addFloatField(t);
                } else if(isBoolean(t.lineValue)) {
                    addBooleanField(t);
                } else {
                    addTag(t);
                }
            }
        }
        return m;
    }

    private void addTag(final Triplet t) {
        t.measurement.addTag(new Tag(t.headValue, t.lineValue));
    }

    private void addFloatField(final Triplet t) {
        t.measurement.addField(new Field(t.headValue, Double.parseDouble(t.lineValue)));
    }

    private void addIntField(final Triplet t) {
        t.measurement.addField(new Field(t.headValue, Integer.parseInt(t.lineValue)));
    }

    private void addBooleanField(final Triplet t) {
        t.measurement.addField(new Field(t.headValue, Boolean.parseBoolean(t.lineValue)));
    }

    private boolean isForceFloat(final String s) {
        return asFloats != null && Arrays.asList(asFloats).contains(s);
    }

    private boolean isForceString(final String s) {
        return asString != null && Arrays.asList(asString).contains(s);
    }

    private boolean isForceBoolean(final String s) {
        return asBoolean != null && Arrays.asList(asBoolean).contains(s);
    }

    private boolean isForceInt(final String s) {
        return asInt != null && Arrays.asList(asInt).contains(s);
    }

    private boolean isInteger(final String s) {
        return intPattern.matcher(s).matches();
    }

    private boolean isBoolean(final String s) {
        return booleanPattern.matcher(s).matches();
    }

    private boolean isFloat(final String s) {
        return floatPattern.matcher(s).matches();
    }

}
