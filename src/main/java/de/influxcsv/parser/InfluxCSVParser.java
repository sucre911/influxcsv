package de.influxcsv.parser;

import de.influxcsv.influxdb.data.Measurement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class InfluxCSVParser {
    private static Logger LOG = LoggerFactory.getLogger(InfluxCSVParser.class);

    public void parse(final String filename, final String[] asFloats, final String[] asInts, final String[] asBooleans, final String[] asStrings) throws IOException {
        LOG.info("parse filename: {}", filename);
        final List<String[]> lines = new InfluxCSVReader().parse(filename);
        final MeasurementFactory measurementFactory = new MeasurementFactory(asFloats, asInts, asBooleans, asStrings);
        final List<Measurement> measurements = lines.stream()
                .skip(1)
                .filter(line -> lines.get(0).length == line.length)
                .map(line -> measurementFactory.buildMeasurement(lines.get(0), line))
                .collect(Collectors.toList());
        measurements.forEach(m -> System.out.println(m.toString()));
    }

}
