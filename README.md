# README #

This is a standalone application for converting the influxdb csv export to the influxdb line protocol.

The line protocol data types are automatically recognized from the csv export, but they can optionally be overwritten with a command line parameter.

### How do I get set up? ###

mvn clean package

### Usage: ###

`java -jar target/uber-influxcsv-1.0-SNAPSHOT.jar`

`-asBoolean <arg>   (optional) header names which force interpreted as Boolean`

`-asFloat <arg>     (optional) header names which force interpreted as Float`

`-asInt <arg>       (optional) header names which force interpreted as Int`

`-asString <arg>    (optional) header names which force interpreted as String`

`-csv <arg>         file which should converted from csv to line protocol`


### Workflow example ###

* export measurement with:

`influx -database '<DATABASE_NAME>' -host 'localhost' -execute 'SELECT * FROM <MEASUREMENT>' -format 'csv' > export.csv`

* manipulate / clean up data
* convert to line protocol with:

`java -jar target/uber-influxcsv-1.0-SNAPSHOT.jar -csv export.csv > export.lp`

* drop old measurement
* re-import with
 
`cat  export.lp > /dev/udp/localhost/8089`
